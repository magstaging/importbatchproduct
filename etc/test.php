<?php

class Alternative extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        $this->getResponse()->setHeader('content-type', 'text/plain');
        $this->getResponse()->appendBody('this is a plain response');
    }
}