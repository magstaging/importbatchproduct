<?php

namespace Mbs\ImportProduct\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportBatchProduct extends Command
{
    /**
     * @var \Mbs\ImportProduct\Model\ProductHandler
     */
    private $productHandler;
    /**
     * @var \Mbs\ImportProduct\Model\ProductDataFinder
     */
    private $productDataFinder;
    /**
     * @var \Mbs\ImportProduct\Model\StockDataHandler
     */
    private $stockDataHandler;
    /**
     * @var \Mbs\ImportProduct\Model\MediaFileHandler
     */
    private $mediaFileHandler;
    /**
     * @var \Mbs\ImportProduct\Model\IndexHandler
     */
    private $indexHandler;

    public function __construct(
        \Mbs\ImportProduct\Model\ProductHandler $productCollectionHandler,
        \Mbs\ImportProduct\Model\ProductDataFinder $productDataFinder,
        \Mbs\ImportProduct\Model\StockDataHandler $stockDataHandler,
        \Mbs\ImportProduct\Model\MediaFileHandler $mediaFileHandler,
        \Mbs\ImportProduct\Model\IndexHandler $indexHandler,
        string $name = null
    ) {
        parent::__construct($name);
        $this->productHandler = $productCollectionHandler;
        $this->productDataFinder = $productDataFinder;
        $this->stockDataHandler = $stockDataHandler;
        $this->mediaFileHandler = $mediaFileHandler;
        $this->indexHandler = $indexHandler;
    }

    protected function configure()
    {
        $this->setName('mbs:import:product')
            ->setDescription('Import product fast');

        $this->addArgument('product_ids', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $productList = $this->productHandler->getProducts($this->getProductIds($input));

        $simpleAttributes = [
            'name',
            'status',
            'weight',
            'visibility',
            'price'
        ];

        $i = 0;
        foreach ($productList as $product) {
            $this->productHandler->updateSku($product, $this->productDataFinder->getProductData('sku', $product->getId()));

            foreach ($simpleAttributes as $simpleAttribute) {
                $product->setData($simpleAttribute, $this->productDataFinder->getProductData($simpleAttribute, $product->getId()));
                $product->getResource()->saveAttribute($product, $simpleAttribute);
            }

            $this->stockDataHandler->saveStockData(
                $product->getId(),
                $this->productDataFinder->getStockData());
            $this->mediaFileHandler->saveMediaFile($product);

            $output->writeln(sprintf('%s: sku: %s done', ++$i, $product->getSku()));
        }

        //$this->indexHandler->runAllIndex();

        $output->writeln('product import complete');
    }

    /**
     * @param InputInterface $input
     * @return mixed|string|string[]|null
     */
    protected function getProductIds(InputInterface $input)
    {
        if (strpos($input->getArgument('product_ids'), ',') !== false) {
            return explode(',', $input->getArgument('product_ids'));
        }

        return [(int)$input->getArgument('product_ids')];
    }
}