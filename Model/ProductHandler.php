<?php

namespace Mbs\ImportProduct\Model;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\ResourceConnection;

class ProductHandler
{
    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        CollectionFactory $productCollectionFactory,
        ResourceConnection $resourceConnection
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->resourceConnection = $resourceConnection;
    }

    public function getProducts(array $productIds)
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addIdFilter($productIds);
        $collection->setStoreId(0);

        return $collection;
    }

    public function updateSku($product, string $sku)
    {
        $connection = $this->resourceConnection->getConnection();

        $connection->update(
            $this->resourceConnection->getTableName('catalog_product_entity'),
            ['sku' => $sku],
            ['entity_id = ?' => (int)$product->getId()]
        );
    }
}