<?php

namespace Mbs\ImportProduct\Model;

class ProductDataFinder
{
    public function getProductData(string $attributeCode, int $productId)
    {
        $productData = $this->getProductRandomData($productId);
        if (isset($productData[$attributeCode])) {
            return $productData[$attributeCode];
        }

        return '';
    }

    public function getStockData()
    {
        return [
            'use_config_manage_stock' => 0,
            'manage_stock' => rand(0,1),
            'is_in_stock' => rand(0,1),
            'qty' => rand(1, 456)
        ];
    }

    private function getProductRandomData(int $productId)
    {
        return [
            'sku' => sprintf('SKUBAT%s', $productId),
            'name' => sprintf('Name for product %s', $productId),
            'status' => rand(0,1),
            'weight' => rand(1, 456),
            'visibility' => rand(0, 4),
            'price' => rand(3, 67)
        ];
    }
}