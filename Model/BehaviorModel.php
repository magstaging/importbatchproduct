<?php

namespace Mbs\ImportProduct\Model;

use Magento\ImportExport\Model\Import;

class BehaviorModel extends \Magento\ImportExport\Model\Source\Import\AbstractBehavior
{
    public function toArray()
    {
        return [
            Import::BEHAVIOR_REPLACE => __('Update')
        ];
    }

    public function getCode()
    {
        return 'update_only';
    }
}