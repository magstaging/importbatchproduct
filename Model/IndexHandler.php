<?php

namespace Mbs\ImportProduct\Model;

class IndexHandler
{
    /**
     * @var \Magento\Framework\Indexer\ConfigInterface
     */
    private $config;
    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    private $indexerFactory;

    public function __construct(
        \Magento\Framework\Indexer\ConfigInterface $config,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory
    ) {
        $this->config = $config;
        $this->indexerFactory = $indexerFactory;
    }

    public function runAllIndex()
    {
        foreach (array_keys($this->config->getIndexers()) as $indexerId) {
            $indexer = $this->indexerFactory->create()->load($indexerId);
            $indexer->reindexAll();
        }
    }
}