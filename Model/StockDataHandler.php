<?php


namespace Mbs\ImportProduct\Model;


use Magento\CatalogInventory\Model\StockRegistry;

class StockDataHandler
{
    /**
     * @var StockRegistry
     */
    private $stockRegistry;

    public function __construct(
        StockRegistry $stockRegistry
    ) {
        $this->stockRegistry = $stockRegistry;
    }

    public function saveStockData(int $productId, array $stockData)
    {
        $stockItem = $this->stockRegistry->getStockItem($productId);
        $stockItem->setQty($stockData['qty']);
        $stockItem->setIsInStock($stockData['is_in_stock']);
        $stockItem->setManageStock($stockData['manage_stock']);
        $stockItem->setUseConfigManageStock($stockData['use_config_manage_stock']);
        $stockItem->save();
    }
}