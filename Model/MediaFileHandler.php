<?php

namespace Mbs\ImportProduct\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\File\Uploader;

class MediaFileHandler
{
    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;
    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    private $file;
    /**
     * @var Uploader
     */
    private $uploader;
    /**
     * @var \Magento\Framework\File\UploaderFactory
     */
    private $uploaderFactory;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filesystem\Io\File $file
    ) {
        $this->filesystem = $filesystem;
        $this->file = $file;
    }

    public function saveMediaFile(\Magento\Catalog\Model\Product $product)
    {
        $imageUrl = sprintf("https://example.com/img%s.jpg", $product->getId());
        $imageName = baseName($imageUrl);
        $tmpDir = $this->getMediaDirTmpDir();
        $newFileName = $tmpDir . $imageName;
        $result = $this->file->fileExists($newFileName);

        if ($result) {
            $discretionPath = $this->getDispersionPathForImage($imageName);
            $newImageValue = $discretionPath . DIRECTORY_SEPARATOR . $imageName;
            $product->setData('small_image', $newImageValue);
            $product->setData('thumbnail', $newImageValue);
            $product->setData('image', $newImageValue);
            foreach (['small_image', 'thumbnail', 'image'] as $simpleAttribute) {
                $product->getResource()->saveAttribute($product, $simpleAttribute);
            }

            $destinationFolder = $this->filesystem->getDirectoryRead(
                DirectoryList::MEDIA
            )->getAbsolutePath(
                'catalog/product' . $discretionPath
            );

            if (substr($destinationFolder, -1) == '/') {
                $destinationFolder = substr($destinationFolder, 0, -1);
            }

            if (!(@is_dir($destinationFolder)
                || @mkdir($destinationFolder, 0777, true)
            )) {
                throw new FileSystemException(__('Unable to create directory %1.', $destinationFolder));
            }

            try {
                $destinationPath = $destinationFolder . DIRECTORY_SEPARATOR . $imageName;
                rename($newFileName, $destinationPath);
                chmod($destinationPath, 0777);
            } catch (\Exception $e) {
            }
        }
    }

    private function getMediaDirTmpDir()
    {
        return $this->filesystem
            ->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('tmp/');
    }

    /**
     * @param string $imageName
     * @return mixed
     */
    private function getDispersionPathForImage(string $imageName)
    {
        return Uploader::getDispersionPath($imageName);
    }
}